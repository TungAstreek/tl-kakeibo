import sys
import os
import calendar
import re
from string import punctuation
from random import choice
from datetime import datetime
from google.cloud import datastore
from flask import (
    Flask, request, abort
)
from linebot import (
    LineBotApi, WebhookHandler
)
from linebot.models import (
    MessageEvent, TextMessage, TextSendMessage
)
from linebot.exceptions import (
    InvalidSignatureError, LineBotApiError
)


accepted_users = {
    "U19f4fa7c258042ef3c0c6d8038da8741": "Tùng Lic",
    "Ud28abc01181674b51f12b704209169b9": "Ly zjp"
}


categories = ["siêu thị", "ăn ngoài", "giải trí", "đi lại", "sức khoẻ", "mua sắm", "quà", "cố định", "khác"]

invalid_user_msg = "Who are you???"

dont_understand_msg = """Câu này khó quá không biết trả lời
Hãy gõ "help" để biết các câu được hỗ trợ
"""

help_msg = """Hãy sử dụng một trong các câu sau

1. Thêm|Add <Content> <Amount> <Category>
=> Thêm một khoản chi
Ví dụ: Thêm mua trà sữa cho vợ 100000 ăn ngoài

2. Xem|Show <MonthYear>
=> Xem tất cả các khoản chi trong 1 tháng nào đó
Ví dụ: Xem 092019

3. Xem <Month Year> <Category>
=> Xem tất cả các khoản chi trong 1 tháng nào đó liên quan đến 1 category nào đó.
Ví dụ: Xem 092019 giải trí
"""

success_msg = "Đã thêm thành công khoản chi sau"

nothing_to_show_msg = "Không có khoản chi nào liên quan 🧐"

remind_msg = "Hãy chi tiêu hợp lý.\n頑張りましょう💪🏻"


app = Flask(__name__)

datastore_client = datastore.Client()

channel_secret = os.getenv('LINE_CHANNEL_SECRET', None)
channel_access_token = os.getenv('LINE_CHANNEL_ACCESS_TOKEN', None)
if channel_secret is None or channel_access_token is None:
    abort(500)

line_bot_api = LineBotApi(channel_access_token)
handler = WebhookHandler(channel_secret)

@app.route("/callback", methods=['POST'])
def callback():
    # get X-Line-Signature header value
    signature = request.headers['X-Line-Signature']
    # get request body as text
    body = request.get_data(as_text=True)
    # handle webhook body
    try:
        handler.handle(body, signature)
    except LineBotApiError as e:
        print("Got exception from LINE Messaging API: %s\n" % e.message)
        for m in e.error.details:
            print("  %s: %s" % (m.property, m.message))
        print("\n")
    except InvalidSignatureError:
        abort(400)

    return 'OK'

@handler.add(MessageEvent, message=TextMessage)
def handle_text_message(event):
    sent_profile = line_bot_api.get_profile(event.source.user_id)
    text = event.message.text.lower().strip()
    add_pattern = re.compile(r"^[thêm|add]+ ([\S\s]+) (\d+) ([\S\s]+)$")
    show_pattern = re.compile(r"^[xem|show]+ (\d+)(\s)*([\S\s]*)$")
    add_match = add_pattern.match(text)
    show_match = show_pattern.match(text)
    if sent_profile.user_id not in accepted_users:
        line_bot_api.reply_message(
            event.reply_token,
            TextSendMessage(text=invalid_user_msg)
        )
        return

    if add_match:
        process_add(event, add_match, sent_profile)

    elif show_match:
        process_show(event, show_match)

    elif text.lower() == "help":
        reply_help(event)

    else:
        reply_not_understand(event)

def process_add(event, matches, sent_profile):
    transaction = datastore.Entity(
        key=datastore_client.key('transaction'),
        exclude_from_indexes=['amount']
    )
    content = matches.group(1)
    amount = int(matches.group(2))
    category = matches.group(3)
    if category in categories:
        transaction.update({
            'time': datetime.now().replace(hour=0, minute=0, second=0, microsecond=0),
            'content': content,
            'amount': amount,
            'category': category
        })
        datastore_client.put(transaction)
        details = "{:%d %B %Y} {}: {:_} ({})".format(
            transaction['time'],
            transaction['content'],
            transaction['amount'],
            transaction['category']
        )
        line_bot_api.reply_message(
            event.reply_token,
            [
                TextSendMessage(text=success_msg),
                TextSendMessage(text=details),
                TextSendMessage(text=remind_msg)
            ]
        )
        sent_name = accepted_users[sent_profile.user_id]
        accepted_ids = list(accepted_users.keys())
        other_id = [i for i in accepted_ids if i != sent_profile.user_id][0]
        line_bot_api.push_message(
            other_id
            ,
            [
                TextSendMessage(text="{} vừa thêm 1 khoản chi sau".format(sent_name)),
                TextSendMessage(text=details)
            ]
        )
    else:
        invalid_category_msg = "Category【{}】không hợp lệ.\nCác category được cho phép là\n{}".format(category, "\n".join(categories))
        line_bot_api.reply_message(
            event.reply_token,
            TextSendMessage(text=invalid_category_msg)
        ) 

def process_show(event, matches):
    transactions = query_transactions(matches)
    if len(transactions) == 0:
        reply_nothing_to_show(event)
    else:
        total = sum([x['amount'] for x in transactions])
        spends_msg = "\n".join(
            ["-{:%d %B} {}: {:_} ({})".format(x['time'], x['content'], x['amount'], x['category']) for x in transactions]
        )[-2000:]
        line_bot_api.reply_message(
            event.reply_token,
            [
                TextSendMessage(text=spends_msg),
                TextSendMessage(text="Total spend: {:_}".format(total))
            ]
        )

def query_transactions(matches):
    date = matches.group(1)
    month = int(date[:2])
    year = int(date[2:])

    last_day_of_month = calendar.monthrange(year, month)[1]
    beginning_of_month = datetime(year=year, month=month, day=1)
    end_of_month = datetime(year=year, month=month, day=last_day_of_month)

    query = datastore_client.query(kind='transaction')
    query.add_filter('time', '>=', beginning_of_month)
    query.add_filter('time', '<=', end_of_month)

    category = matches.group(3)
    if category:
        query.add_filter('category', '=', category)

    return list(query.fetch())

def reply_not_understand(event):
    line_bot_api.reply_message(
        event.reply_token,
        TextSendMessage(text=dont_understand_msg)
    )

def reply_help(event):
    line_bot_api.reply_message(
            event.reply_token,
            TextSendMessage(text=help_msg),
    )

def reply_nothing_to_show(event):
    line_bot_api.reply_message(
        event.reply_token,
        TextSendMessage(text=nothing_to_show_msg)
    )


if __name__ == "__main__":
    app.run()
